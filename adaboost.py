import pandas as pd
import numpy as np
from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import train_test_split
from sklearn.datasets import make_hastie_10_2
import matplotlib.pyplot as plt


""" HELPER FUNCTION: GET ERROR RATE ========================================="""
def get_error_rate(pred, Y):
    return sum(pred != Y) / float(len(Y))

""" HELPER FUNCTION: PRINT ERROR RATE ======================================="""
def print_error_rate(err):
    print('Error rate: Training: %.4f - Test: %.4f' % err)

""" HELPER FUNCTION: GENERIC CLASSIFIER ====================================="""
def generic_clf(Y_train, X_train, Y_test, X_test, clf):
    clf.fit(X_train,Y_train)
    pred_train = clf.predict(X_train)
    pred_test = clf.predict(X_test)
    return get_error_rate(pred_train, Y_train), \
           get_error_rate(pred_test, Y_test)


""" ADABOOST IMPLEMENTATION ================================================="""
def adaboost_clf(Y_train, X_train, Y_test, X_test, clf, T, margin=False):
    n_train, n_test = len(X_train), len(X_test)
    # Initialize weights
    w = np.ones(n_train) / n_train
    pred_train, pred_test = [np.zeros(n_train), np.zeros(n_test)]

    all_alphas = []
    for i in range(T):
        # Fit a classifier with the specific weights
        clf.fit(X_train, Y_train, sample_weight = w)
        pred_train_i = clf.predict(X_train)
        pred_test_i = clf.predict(X_test)
        # Indicator function                
        miss = [int(x) for x in (pred_train_i != Y_train)]

        # Equivalent with 1/-1 to update weights
        miss2 = [x if x==1 else -1 for x in miss]
        # Error
        err_m = np.dot(w,miss) / sum(w)
        # Alpha
        alpha_m = 0.5 * np.log( (1 - err_m) / float(err_m))
        #regularize
        Z_t = 2*np.sqrt(err_m*(1-err_m))
        # New weights        
        w = np.multiply(w, np.exp([float(x) * alpha_m for x in miss2]))/Z_t
        # Add to prediction
        pred_train = [sum(x) for x in zip(pred_train, 
                                          [x * alpha_m for x in pred_train_i])]
        pred_test = [sum(x) for x in zip(pred_test, 
                                         [x * alpha_m for x in pred_test_i])]
        all_alphas.append(alpha_m)

    pred_train_raw = pred_train
    pred_train, pred_test = np.sign(pred_train), np.sign(pred_test)
    # Return error rate in train and test set
    if margin == False:
        return_val  = (get_error_rate(pred_train, Y_train), get_error_rate(pred_test, Y_test))        
    else:
        norm = np.linalg.norm(all_alphas, ord=1)
        margin_train = [(_[0]*_[1])/norm for _ in zip(Y_train, pred_train_raw)]
        #print(margin_train[:10], margin_train[40:45], margin_train[100:110])
        #exit()
        # print (margin_train[:50])
        return_val  = (get_error_rate(pred_train, Y_train), get_error_rate(pred_test, Y_test),
                                margin_train)
    return return_val

""" ADABOOST MARGIN ========================="""
def margin_adaboost(Y_train, X_train, Y_test, X_test, clf, T, p, margin=None):
    #return adaboost_clf(Y_train, X_train, Y_test, X_test, clf, T, margin)    
    n_train, n_test = len(X_train), len(X_test)
    # Initialize weights
    w = np.ones(n_train) / n_train
    pred_train, pred_test = [np.zeros(n_train), np.zeros(n_test)]

    all_alphas = []
    for i in range(T):
        clf.fit(X_train, Y_train, sample_weight = w)
        pred_train_i = clf.predict(X_train)
        pred_test_i = clf.predict(X_test)
        
        
        miss = [int(x) for x in (pred_train_i != Y_train)]
        miss2 = [x if x==1 else -1 for x in miss]
        err_m = np.dot(w,miss) / sum(w)
        alpha_m = 0.5 * np.log(((1 - err_m) * (1 - p)) / (float(err_m) * float(1 + p)))        
        
        Z_t = (1-err_m)*((((1+p)*err_m)/((1-p)*(1-err_m)))**(0.5*(1-p))) + \
                            (err_m * ((((1-p)*(1-err_m))/((1+p)*err_m))**(0.5*(1+p))))
        # New weights        
        w = np.multiply(w, np.exp([(float(x) * alpha_m) + (p * alpha_m) for x in miss2]))/Z_t
        # Add to prediction
        pred_train = [sum(x) for x in zip(pred_train, 
                                          [x * alpha_m for x in pred_train_i])]
        pred_test = [sum(x) for x in zip(pred_test, 
                                         [x * alpha_m for x in pred_test_i])]
        all_alphas.append(alpha_m)

    pred_train_raw = pred_train
    pred_train, pred_test = np.sign(pred_train), np.sign(pred_test)
    # Return error rate in train and test set
    if margin is None:
        return_val  = (get_error_rate(pred_train, Y_train), get_error_rate(pred_test, Y_test))
    else:
        norm = np.linalg.norm(all_alphas, ord=1)
        margin_train = [(_[0]*_[1])/norm for _ in zip(Y_train, pred_train_raw)]
        return_val  = (get_error_rate(pred_train, Y_train), get_error_rate(pred_test, Y_test),
                                margin_train)
    return return_val




""" PLOT FUNCTION ==========================================================="""
def plot_error_rate(er_train, er_test, plot_name):
    df_error = pd.DataFrame([er_train, er_test]).T
    df_error.columns = ['Training', 'Test']
    plot1 = df_error.plot(linewidth = 3, figsize = (8,6),
            color = ['lightblue', 'darkblue'], grid = True)
    plot1.set_xlabel('Number of iterations', fontsize = 12)
    plot1.set_xticklabels(range(0,450,50))
    plot1.set_ylabel('Error rate', fontsize = 12)
    plot1.set_title('Error rate vs number of iterations', fontsize = 16)
    plt.axhline(y=er_test[0], linewidth=1, color = 'red', ls = 'dashed')
    plt.savefig('%s.png'%plot_name)


""" MAIN SCRIPT ============================================================="""
if __name__ == '__main__':
    
    # Read data
    x, y = make_hastie_10_2()
    df = pd.DataFrame(x)
    df['Y'] = y

    # Split into training and test set
    train, test = train_test_split(df, test_size = 0.2)
    X_train, Y_train = train.ix[:,:-1], train.ix[:,-1]
    X_test, Y_test = test.ix[:,:-1], test.ix[:,-1]
    
    # Fit a simple decision tree first
    clf_tree = DecisionTreeClassifier(max_depth = 1, random_state = 1)
    er_tree = generic_clf(Y_train, X_train, Y_test, X_test, clf_tree)
    
    # Fit Adaboost classifier using a decision tree as base estimator
    # Test with different number of iterations
    er_train, er_test = [er_tree[0]], [er_tree[1]]
    x_range = [10]#range(10)#, 410, 10)
    for i in x_range:    
        er_i = adaboost_clf(Y_train, X_train, Y_test, X_test, i, clf_tree)
        print(er_i)
        er_train.append(er_i[0])
        er_test.append(er_i[1])
    print(er_train)
    print(len(er_train))
    # Compare error rate vs number of iterations
    plot_error_rate(er_train, er_test, 'round10')
