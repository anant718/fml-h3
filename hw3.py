
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
import matplotlib.pyplot as plt
import numpy as np
from adaboost import *

#Read dataset file using dataframe
dataset = pd.read_csv("data/spam.csv",header=None)


#Normalize data
scaler = MinMaxScaler()
#Partition training & test
trainingdata =  dataset.head(3000)
testdata = dataset.tail(1601)
scaler.fit(trainingdata)
scaled_values = scaler.transform(trainingdata)
trainingdata.loc[:,:] = scaled_values

scaled_values = scaler.transform(testdata)
testdata.loc[:,:] = scaled_values

x = trainingdata[trainingdata.columns[:57]].values
y = trainingdata[trainingdata.columns[57]].values
x_test = testdata[testdata.columns[:57]].values
y_test = testdata[testdata.columns[57]].values

y[y==0]=-1
y_test[y_test==0]=-1

def cross_val(t, p):    
    cv_train_error, cv_test_error = [], []
    for i in range(10):
        val_start = i*300
        val_end = val_start+300
        x_val, y_val = x[val_start: val_end], y[val_start:val_end] 
        x_batch = np.concatenate([x[0:val_start], x[val_end:]], axis=0) 
        y_batch = np.concatenate([y[0:val_start], y[val_end:]], axis=0)         

        base_clf = DecisionTreeClassifier(max_depth = 1, random_state = 1)        
        er_train, er_test = margin_adaboost(y_batch, x_batch, y_val, x_val, base_clf, t, p)        
        cv_train_error.append(er_train)
        cv_test_error.append(er_test)
    
    return(np.mean(cv_train_error), np.mean(cv_test_error))    

def find_best_params(T_range, p_range):
    min_error = 100
    for t in T_range:
        for p in p_range:            
            train_error, test_error = cross_val(t, 2.0**p)
            
            print(t, p, ":", train_error, test_error)
            if test_error < min_error:
                min_error = test_error
                min_param = (t, p)
    
    return min_param

if __name__ == "__main__":
    T_range = [100, 200, 500, 1000]
    p_range = range(-10, 0)
    
    min_param = find_best_params(T_range, p_range)    
    print("Best T & p: ", min_param)
    min_param = (1000, -10)    

    base_clf = DecisionTreeClassifier(max_depth = 1, random_state = 1)
    best_train_error, best_test_error = margin_adaboost(y, x, y_test, x_test, base_clf, min_param[0], 2.0**min_param[1])
    print("margin_adaboost results:", best_train_error, best_test_error)
    ## margin_adaboost results: 0.044 0.054965646471

    best_train_error, best_test_error = adaboost_clf(y, x, y_test, x_test, base_clf, min_param[0])
    print("regular_adaboost results:", best_train_error, best_test_error)
    ## regular_adaboost results: 0.0406666666667 0.053716427233


    thetas = [0, 0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 0.15, 0.2]#, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]    
    #thetas = [0.0, 0.01, 0.1, 0.5]
    thetas = np.linspace(0,1,20)
    margin_x, margin_y = [], []
    for i in thetas:
        base_clf = DecisionTreeClassifier(max_depth = 1, random_state = 1)
        best_train_error, best_test_error, margins = adaboost_clf(y, x, y_test, x_test, 
                                    base_clf, 100, True)
        margin_y.append(sum([1 for _ in margins if _ <= round(i, 2)]))
        margin_x.append(round(i, 2))        
    print(margin_x)
    print(margin_y)
    print("=========")
    plt.plot(margin_x, margin_y, label='Adaboost')

    margin_x, margin_y = [], []
    for i in thetas:
        base_clf = DecisionTreeClassifier(max_depth = 1, random_state = 1)
        best_train_error, best_test_error, margins = margin_adaboost(y, x, y_test, x_test, 
                                    base_clf, 500, 2.0**min_param[1], True)
        margin_y.append(sum([1 for _ in margins if _ <= round(i, 2)]))
        margin_x.append(round(i, 2))        
    print(margin_x)
    print(margin_y)
    plt.plot(margin_x, margin_y, label='A_p')
    plt.ylabel('No. of points')
    plt.xlabel('Margin')
    plt.grid(True)
    plt.legend(loc=4)
    plt.title('Solution 7')
    plt.savefig('plots/prob_7.png')

    '''
    0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.1, 0.15, 0.2]
    [184, 184, 184, 184, 184, 184, 3000, 3000, 3000]
    =========
    [0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.1, 0.15, 0.2]
    [145, 145, 145, 145, 145, 145, 3000, 3000, 3000]
    '''



'''
CV report
100 -10 : 0.0598148148148 0.0646666666667
100 -9 : 0.0607777777778 0.0653333333333
100 -8 : 0.0605185185185 0.0646666666667
100 -7 : 0.0624814814815 0.0673333333333
100 -6 : 0.0654444444444 0.0703333333333
100 -5 : 0.0815185185185 0.083
100 -4 : 0.093037037037 0.100333333333
100 -3 : 0.0938148148148 0.102333333333
100 -2 : 0.113148148148 0.119
100 -1 : 0.205777777778 0.208666666667
200 -10 : 0.053962962963 0.0633333333333
200 -9 : 0.0542962962963 0.0633333333333
200 -8 : 0.0556666666667 0.0636666666667
200 -7 : 0.0577777777778 0.0663333333333
200 -6 : 0.0640740740741 0.07
200 -5 : 0.0815185185185 0.083
200 -4 : 0.093037037037 0.100333333333
200 -3 : 0.0938148148148 0.102333333333
200 -2 : 0.113148148148 0.119
200 -1 : 0.205777777778 0.208666666667
500 -10 : 0.0474074074074 0.059
500 -9 : 0.0478148148148 0.0596666666667
500 -8 : 0.0498148148148 0.0613333333333
500 -7 : 0.0532962962963 0.064
500 -6 : 0.0634444444444 0.0683333333333
500 -5 : 0.0815185185185 0.083
500 -4 : 0.093037037037 0.100333333333
500 -3 : 0.0938148148148 0.102333333333
500 -2 : 0.113148148148 0.119
500 -1 : 0.205777777778 0.208666666667
1000 -10 : 0.0405925925926 0.058
1000 -9 : 0.0416296296296 0.0593333333333
1000 -8 : 0.0445185185185 0.0593333333333
1000 -7 : 0.0509259259259 0.0606666666667
1000 -6 : 0.0634444444444 0.0683333333333
1000 -5 : 0.0815185185185 0.083
1000 -4 : 0.093037037037 0.100333333333
1000 -3 : 0.0938148148148 0.102333333333
1000 -2 : 0.113148148148 0.119
1000 -1 : 0.205777777778 0.208666666667
'''    
